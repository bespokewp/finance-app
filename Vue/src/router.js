import Vue from 'vue'
import Router from 'vue-router'
import ViewClients from './views/clients/ViewClients.vue'
import ViewClient from './views/clients/ViewClient.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'clients',
      component: ViewClients
    },
    {
      path: '/clients/:id',
      name: 'viewClient',
      component: ViewClient,
      props: true
    },
    {
      path: '/jobs',
      name: 'jobs',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "jobs" */ './views/jobs/ViewJobs.vue')
    },
    {
      path: '/invoices',
      name: 'invoices',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "invoices" */ './views/invoices/ViewInvoices.vue')
    }
  ]
})
