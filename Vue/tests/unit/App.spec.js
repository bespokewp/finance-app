import { shallowMount } from '@vue/test-utils'
import App from '@/App.vue'
import { expect } from 'chai';

describe("App.vue", () => {

  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(App, {
      stubs: ['router-view', 'router-link']
    })
  });

  it("Module has the expected html structure", () => {
    expect(wrapper.text()).to.include('You found it');
  });


  it('simple passing test', () => {
    expect(1).to.equal(1);
  });


});