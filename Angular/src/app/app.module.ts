import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';

import { ViewClientsComponent } from './views/clients/view-clients/view-clients.component';
import { ViewClientComponent } from './views/clients/view-client/view-client.component';
import { AddClientComponent } from './views/clients/add-client/add-client.component';
import { EditClientComponent } from './views/clients/edit-client/edit-client.component';
import { AddEditClientComponent } from './views/clients/forms/add-edit-client/add-edit-client.component';

import { ViewJobsComponent } from './views/jobs/view-jobs/view-jobs.component';
import { ViewJobComponent } from './views/jobs/view-job/view-job.component';
import { AddJobComponent } from './views/jobs/add-job/add-job.component';
import { EditJobComponent } from './views/jobs/edit-job/edit-job.component';
import { AddEditJobComponent } from './views/jobs/forms/add-edit-job/add-edit-job.component';

import { ViewInvoicesComponent } from './views/invoices/view-invoices/view-invoices.component';
import { AddInvoiceComponent } from './views/invoices/add-invoice/add-invoice.component';
import { ViewInvoiceComponent } from './views/invoices/view-invoice/view-invoice.component';
import { EditInvoiceComponent } from './views/invoices/edit-invoice/edit-invoice.component';
import { AddEditInvoiceComponent } from './views/invoices/forms/add-edit-invoice/add-edit-invoice.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,

    ViewClientsComponent,
    ViewClientComponent,
    AddClientComponent,
    EditClientComponent,

    ViewJobsComponent,
    ViewJobComponent,
    AddJobComponent,
    EditJobComponent,

    ViewInvoicesComponent,
    AddInvoiceComponent,
    ViewInvoiceComponent,

    AddEditClientComponent,
    AddEditInvoiceComponent,
    AddEditJobComponent,
    EditInvoiceComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
