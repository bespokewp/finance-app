export class Job {

  public id: number;
  public client: object;
  public client_id: number;
  public title: string;
  public description: string;
  public date_accepted: number;
  public date_due: number;
  public cost_quoted: number;
  public completed: boolean;

  constructor(
  ) {
    this.client = {};
  }

}
