import {Client} from "./client";

export class Invoice {

  public id: number;
  public client: Client;
  public client_id: number;
  public date_sent: number;
  public date_due: number;
  public date_paid: number;
  public paid: boolean;
  public jobs: number[];

  constructor() {}

}
