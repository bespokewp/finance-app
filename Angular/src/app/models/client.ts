export class Client {

  public id: number;
  public name: string;
  public work_value: number;
  public client_since: number;
  public address: string[];
  public phone_number: string;

  constructor() {
    this.address = [];
    this.work_value = 0;
  }
}
