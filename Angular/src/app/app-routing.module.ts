import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// import { DashboardComponent } from './views/dashboard/dashboard.component';

// client components
import { ViewClientsComponent } from './views/clients/view-clients/view-clients.component';
import { ViewClientComponent } from './views/clients/view-client/view-client.component';
import { AddClientComponent } from './views/clients/add-client/add-client.component';
import { EditClientComponent } from './views/clients/edit-client/edit-client.component';

// jobs components
import {ViewJobsComponent} from './views/jobs/view-jobs/view-jobs.component';
import {ViewJobComponent} from './views/jobs/view-job/view-job.component';
import {AddJobComponent} from './views/jobs/add-job/add-job.component';
import {EditJobComponent} from './views/jobs/edit-job/edit-job.component';

// invoice components
import {ViewInvoicesComponent} from './views/invoices/view-invoices/view-invoices.component';
import {AddInvoiceComponent} from './views/invoices/add-invoice/add-invoice.component';
import {ViewInvoiceComponent} from './views/invoices/view-invoice/view-invoice.component';
import {EditInvoiceComponent} from './views/invoices/edit-invoice/edit-invoice.component';

const routes: Routes = [

    { path: '', redirectTo: '/clients', pathMatch: 'full' },

    // Client relate routes
    { path: 'clients', component: ViewClientsComponent },
    { path: 'clients/add', component: AddClientComponent },
    { path: 'clients/:id/edit', component: EditClientComponent },
    { path: 'clients/:id', component: ViewClientComponent },

    // Job related routes
    { path: 'jobs', component: ViewJobsComponent },
    { path: 'jobs/add', component: AddJobComponent },
    { path: 'jobs/:id/edit', component: EditJobComponent },
    { path: 'jobs/:id', component: ViewJobComponent },

    // Invoice related routes
    { path: 'invoices', component: ViewInvoicesComponent },
    { path: 'invoices/add', component: AddInvoiceComponent },
    { path: 'invoices/:id/edit', component: EditInvoiceComponent },
    { path: 'invoices/:id', component: ViewInvoiceComponent },
];

@NgModule({
    exports: [ RouterModule ],
    imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule {}
