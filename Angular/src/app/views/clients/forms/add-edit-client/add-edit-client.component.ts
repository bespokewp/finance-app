import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import {Client} from '../../../../models/client';
import {ClientsService} from '../../../../services/clients/clients.service';



@Component({
  selector: 'app-add-edit-client',
  templateUrl: './add-edit-client.component.html',
  styleUrls: ['./add-edit-client.component.scss']
})
export class AddEditClientComponent implements OnInit {

  @Input() client: Client;
  @Input() saveForm: boolean|null = null;

  constructor(
    private clientService: ClientsService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getClient();
  }

  getClient() {
    const id = this.route.snapshot.paramMap.get('id');
    if ( id !== null ) {
      this.clientService.getClient( id )
        .subscribe(client => this.client = client);
    }
  }

  onSubmit() {
    if ( this.saveForm === true ) {
      this.save();
    } else if ( this.saveForm === false ) {
      this.update();
    }
  }

  save() {
    this.clientService.addClient( this.client as Client )
      .subscribe( client => {
        this.backToClientsPage();
      } );
  }


  update() {
    this.clientService.updateClient( this.client )
      .subscribe( () => this.backToClientsPage() );
  }

  backToClientsPage() {
    this.router.navigate(['/clients']);
  }

}
