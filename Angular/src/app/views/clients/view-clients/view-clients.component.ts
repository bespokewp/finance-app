import { Component, OnInit } from '@angular/core';
import {ClientsService} from '../../../services/clients/clients.service';
import {Client} from '../../../models/client';
import {ErrorsDebug} from "../../../debug/errors.debug";

@Component({
  selector: 'app-view-clients',
  templateUrl: './view-clients.component.html',
  styleUrls: ['./view-clients.component.scss']
})
export class ViewClientsComponent implements OnInit {

  clients: Client[] = [];
  error;

  constructor(private clientService: ClientsService, private errorHandler: ErrorsDebug ) { }

  ngOnInit() {
      this.getClients();
  }

  getClients(): void {
      this.clientService.getClients()
          .subscribe(
              (clients: Client[] )=> this.clients = clients, // success path
              error => this.errorHandler.handleError(error)
          );
  }

}
