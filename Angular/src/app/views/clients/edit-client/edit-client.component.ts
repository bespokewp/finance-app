import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Client } from '../../../models/client';
import {ClientsService} from '../../../services/clients/clients.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.scss']
})
export class EditClientComponent implements OnInit {

    @Input() client: Client;

    constructor(
        private route: ActivatedRoute,
        private clientService: ClientsService,
        private router: Router,
        private location: Location
    ) { }

    ngOnInit() {
        this.getClient();
    }

    getClient() {
        const id = this.route.snapshot.paramMap.get('id');

        this.clientService.getClient(id)
            .subscribe(client => this.client = client);
    }

    onSubmit() {
      this.clientService.updateClient(this.client)
          .subscribe( () => this.backToClientsPage() );
    }


    backToClientsPage() {
        return this.router.navigate(['/clients']);
    }


    goBack(): void {

      this.location.back();
    }

}
