import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import {Client} from '../../../models/client';
import {ClientsService} from '../../../services/clients/clients.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {

  date: Date  = new Date();
  title = 'Add new client';
  client = new Client();


  constructor(
    private clientService: ClientsService,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {}

  onSubmit() {

    this.clientService.addClient( this.client as Client )
      .subscribe();

    this.client = new Client();

    return this.backToClientsPage();
  }

  backToClientsPage() {
    return this.router.navigate(['/clients']);

  }

  goBack(): void {
    this.location.back();
  }

}
