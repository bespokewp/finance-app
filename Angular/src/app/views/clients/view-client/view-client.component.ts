import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';


import {ClientsService} from '../../../services/clients/clients.service';
import {Client} from '../../../models/client';

@Component({
  selector: 'app-view-client',
  templateUrl: './view-client.component.html',
  styleUrls: ['./view-client.component.scss']
})
export class ViewClientComponent implements OnInit {

  client: Client;

  constructor(
    private route: ActivatedRoute,
    private clientService: ClientsService,
    private location: Location,
  ) { }

  ngOnInit() {
    this.getClient();
  }

  getClient(): void {
    const id = this.route.snapshot.paramMap.get('id');

    this.clientService.getClient(id)
        .subscribe(client => this.client = client);

  }

  goBack(): void {
    this.location.back();
  }
}
