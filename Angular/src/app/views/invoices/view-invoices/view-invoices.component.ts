import { Component, OnInit } from '@angular/core';
import {Invoice} from '../../../models/invoice';
import {InvoicesService} from '../../../services/invoices/invoices.service';

@Component({
  selector: 'app-view-invoices',
  templateUrl: './view-invoices.component.html',
  styleUrls: ['./view-invoices.component.scss']
})
export class ViewInvoicesComponent implements OnInit {

  invoices: Invoice[] = [];

  constructor(private invoiceService: InvoicesService) { }

  ngOnInit() {
    this.getInvoices();
  }

  getInvoices() {
    this.invoiceService.getInvoices()
      .subscribe(invoices => this.invoices = invoices) ;
  }

}
