import {Component, Input, OnInit} from '@angular/core';
import {Invoice} from '../../../models/invoice';
import {InvoicesService} from '../../../services/invoices/invoices.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-edit-invoice',
  templateUrl: './edit-invoice.component.html',
  styleUrls: ['./edit-invoice.component.scss']
})

export class EditInvoiceComponent implements OnInit {

  @Input() invoice : Invoice;

  constructor(
    private invoiceService: InvoicesService,
    private route: ActivatedRoute,
    private location: Location,
  ) { }

  ngOnInit() {
    this.getInvoice();
  }


  getInvoice() {
    const id = this.route.snapshot.paramMap.get('id');
    this.invoiceService.getInvoice( id )
      .subscribe( invoice => this.invoice = invoice );
  }


  goBack(): void {
    this.location.back();
  }

}
