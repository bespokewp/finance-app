import {Component, Input, OnInit} from '@angular/core';
import {Invoice} from '../../../../models/invoice';
import {Client} from '../../../../models/client';
import {ClientsService} from '../../../../services/clients/clients.service';
import {JobsService} from '../../../../services/jobs/jobs.service';
import {Job} from '../../../../models/job';
import {ActivatedRoute, Router} from '@angular/router';
import {InvoicesService} from '../../../../services/invoices/invoices.service';

@Component({
  selector: 'app-add-edit-invoice',
  templateUrl: './add-edit-invoice.component.html',
  styleUrls: ['./add-edit-invoice.component.scss']
})
export class AddEditInvoiceComponent implements OnInit {

  @Input() invoice: Invoice = new Invoice();
  @Input() clientJobs: Job[] = [];
  @Input() client: Client = new Client();
  clients: Client[] = [];
  @Input() saveForm: boolean|null = null;

  constructor(
    private clientService: ClientsService,
    private jobService: JobsService,
    private route: ActivatedRoute,
    private invoiceService: InvoicesService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getClients();
    this.getClientJobs();
  }

  onSubmit() {
    if ( this.saveForm === true ) {
      this.save();
    } else if ( this.saveForm === false ) {
      this.update();
    } else {
      console.log('component must have `saveForm` attribute passed in with boolean value');
    }
  }

  getClients() {
    this.clientService.getClients()
      .subscribe( clients => {
        this.clients = clients;
      } );
  }

  getClientJobs() {
    // todo: only allow this when creating an invoice (currently won't be able to reassign)
    if ( this.invoice.client_id ) {
      this.jobService.getClientJobs( this.invoice.client_id )
        .subscribe( clientJobs => this.clientJobs = clientJobs );
    }
  }

  save() {
    this.invoiceService.addInvoice( this.invoice as Invoice)
      .subscribe( () => this.backToInvoicesPage() );
  }

  update() {
    this.invoiceService.updateInvoice( this.invoice )
      .subscribe( () => this.backToInvoicesPage() );
  }

  backToInvoicesPage() {
    this.router.navigate(['/invoices']);
  }


}
