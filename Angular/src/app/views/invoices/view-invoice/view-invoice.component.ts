import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Route, Router} from '@angular/router';
import { Location } from '@angular/common';


import {Invoice} from '../../../models/invoice';
import {InvoicesService} from '../../../services/invoices/invoices.service';

@Component({
  selector: 'app-view-invoice',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.scss']
})
export class ViewInvoiceComponent implements OnInit {

  invoice: Invoice;

  constructor(
    private route: ActivatedRoute,
    private invoiceService: InvoicesService,
    private location: Location,
    private router: Router
  ) { }

  ngOnInit() {
    this.getInvoice();
  }

  getInvoice(): void {
    const id = this.route.snapshot.paramMap.get('id');

    this.invoiceService.getInvoice(id)
        .subscribe(invoice => this.invoice = invoice);

  }

  goBack(): void {
    this.location.back();
  }


  markAsPaid(): void {
      this.invoice.paid = true;
      this.invoiceService.updateInvoice( this.invoice )
          .subscribe();
  }


  markAsUnpaid(): void {
      this.invoice.paid = false;
      this.invoiceService.updateInvoice( this.invoice )
          .subscribe();
  }


  deleteInvoice(): void {
      this.invoiceService.deleteInvoice( this.invoice )
          .subscribe(() => this.backToInvoicesPage() );
  }


    backToInvoicesPage() {
        this.router.navigate(['/invoices']);
    }
}
