import { Component, OnInit } from '@angular/core';
import {Job} from '../../../models/job';
import {JobsService} from '../../../services/jobs/jobs.service';
import {ClientsService} from '../../../services/clients/clients.service';

@Component({
  selector: 'app-view-jobs',
  templateUrl: './view-jobs.component.html',
  styleUrls: ['./view-jobs.component.scss']
})
export class ViewJobsComponent implements OnInit {

    jobs: Job[] = [];

    constructor( private jobsService: JobsService, private clientsService: ClientsService ) { }

    ngOnInit() {
        this.getJobs();
    }

    getJobs() {
        this.jobsService.getJobs()
            .subscribe(jobs => this.jobs = jobs );

    }

}
