import {Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';

import {Job} from '../../../models/job';
import {JobsService} from '../../../services/jobs/jobs.service';

@Component({
  selector: 'app-edit-job',
  templateUrl: './edit-job.component.html',
  styleUrls: ['./edit-job.component.scss']
})
export class EditJobComponent implements OnInit {

  @Input() job: Job;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private jobService: JobsService,
  ) {}

  ngOnInit() {
    this.getJob();
  }

  getJob() {
    const id = this.route.snapshot.paramMap.get('id');

    this.jobService.getJob(id)
      .subscribe(job => {
        return [this.job = job];
      });
  }

  goBack(): void {
    this.location.back();
  }

}
