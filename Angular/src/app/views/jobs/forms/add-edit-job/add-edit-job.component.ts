import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import {Job} from '../../../../models/job';
import {ClientsService} from '../../../../services/clients/clients.service';
import {Client} from '../../../../models/client';
import {JobsService} from '../../../../services/jobs/jobs.service';



@Component({
  selector: 'app-add-edit-job',
  templateUrl: './add-edit-job.component.html',
  styleUrls: ['./add-edit-job.component.scss']
})
export class AddEditJobComponent implements OnInit {

  @Input() job: Job;
  @Input() interimCost = 0;
  @Input() saveForm: boolean|null = null;
  clients: Client[] = [];

  constructor(
    private clientService: ClientsService,
    private jobService: JobsService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getClients();
    this.interimCost = this.job.cost_quoted / 100;
  }

  getClients() {
    // todo: only return the whole list when creating, otherwise use the information in the job object (disabled field)
    this.clientService.getClients()
      .subscribe( clients => this.clients = clients );


  }

  backToJobsPage() {
    this.router.navigate(['/jobs']);
  }

  onSubmit() {
    this.job.cost_quoted = this.interimCost;

    if ( this.saveForm === true ) {
      this.save();
    } else if( this.saveForm === false ) {
      this.update();
    }
  }

  save() {
    this.jobService.addJob( this.job as Job )
      .subscribe( () => this.router.navigate(['jobs']) );
  }


  update() {
      this.jobService.updateJob(this.job)
        .subscribe( () => this.backToJobsPage() );
  }

}
