import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';

import {Job} from '../../../models/job';
import {JobsService} from '../../../services/jobs/jobs.service';

@Component({
  selector: 'app-view-job',
  templateUrl: './view-job.component.html',
  styleUrls: ['./view-job.component.scss']
})
export class ViewJobComponent implements OnInit {

  job: Job;

  constructor(
    private location: Location,
    private jobService: JobsService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit() {
    this.getJob();
  }

  getJob(): void {
    const id = this.route.snapshot.paramMap.get('id');

    this.jobService.getJob(id)
      .subscribe(job => this.job = job);
  }

  goBack(): void {
    this.location.back();
  }



  /*
    Actions
   */
    markAsComplete(): void {
        this.job.completed = true;
        this.jobService.updateJob( this.job )
            .subscribe();
    }


    markAsIncomplete(): void {
        this.job.completed = false;
        this.jobService.updateJob( this.job )
            .subscribe();
    }


    deleteJob(): void {
        this.jobService.deleteJob( this.job )
            .subscribe(() => this.backToJobsPage() );
    }


    backToJobsPage() {
        this.router.navigate(['/jobs']);
    }

}
