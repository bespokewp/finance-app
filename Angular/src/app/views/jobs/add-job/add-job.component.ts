import {Component, Input, OnInit} from '@angular/core';
import {Location} from '@angular/common';

import {JobsService} from '../../../services/jobs/jobs.service';
import {Job} from '../../../models/job';
import {Router} from '@angular/router';
import {Client} from '../../../models/client';
import {ClientsService} from '../../../services/clients/clients.service';

@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.scss']
})
export class AddJobComponent implements OnInit {

  clients: Client[] = [];
  @Input() currentJob: Job = new Job();

  constructor(
    private location: Location,
    private jobsService: JobsService,
    private clientsService: ClientsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getClients();
  }


  add( payload ) {

    this.jobsService.addJob( payload as Job )
      .subscribe();

    return this.router.navigate(['jobs']);
  }

  goBack(): void {
    this.location.back();
  }

  getClients() {
    this.clientsService.getClients()
      .subscribe( clients => this.clients = clients );
  }
}
