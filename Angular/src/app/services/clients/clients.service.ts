import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Client } from '../../models/client';
import {AppConfig} from "../../app.config";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

    private clientsURL = `${AppConfig.API_ENDPOINT}/clients`;  // URL to web api


    clients: Client[] = [];

    constructor( private http: HttpClient ) { }

    /** GET: clients from the server */
    getClients(): Observable<Client[]> {
        return this.http.get<Client[]>(this.clientsURL);
    }

    /** GET: client from the server based on the ID passed */
    getClient(id: string): Observable<Client> {
        const url = `${this.clientsURL}/${id}`;
        return this.http.get<Client>(url);
    }


    /** POST: add a new client to the server */
    addClient( client: Client): Observable<Client> {
      const url = `${this.clientsURL}/add`;
      return this.http.post<Client>(url, client, httpOptions);
    }

    /** POST: Update the client on the server based on the ID passed */
    updateClient(client: Client): Observable<any> {
      const url = `${this.clientsURL}/${client.id}`
      return this.http.post(url, client, httpOptions);
    }

}
