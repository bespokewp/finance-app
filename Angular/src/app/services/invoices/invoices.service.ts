import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Invoice} from '../../models/invoice';
import {AppConfig} from "../../app.config";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class InvoicesService {

  private invoicesURL = `${AppConfig.API_ENDPOINT}/invoices`;  // URL to web api

  invoices: Invoice[] = [];

  constructor( private http: HttpClient ) {}

  /** GET clients from the server */
  getInvoices(): Observable<Invoice[]> {
    return this.http.get<Invoice[]>(this.invoicesURL);
  }

  getInvoice(id: string): Observable<Invoice> {
    const url = `${this.invoicesURL}/${id}`;
    return this.http.get<Invoice>(url);
  }


  /** POST: add a new invoice to the server */
  addInvoice(invoice: Invoice): Observable<Invoice> {
    const url = `${this.invoicesURL}/add`;
    return this.http.post<Invoice>(url, invoice, httpOptions);
  }

  /** POST: Update the invoice on the server */
  updateInvoice(invoice: Invoice): Observable<any> {
    const url = `${this.invoicesURL}/${invoice.id}`;
    return this.http.post(url, invoice, httpOptions);
  }

  /** DELETE: Delete the invoice on the server */
  deleteInvoice(invoice: Invoice): Observable<any> {
    const url = `${this.invoicesURL}/${invoice.id}`;
    return this.http.delete(url, httpOptions);
  }

}
