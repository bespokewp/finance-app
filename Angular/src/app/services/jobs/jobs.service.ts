import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs/index';
import {Job} from '../../models/job';
import {AppConfig} from "../../app.config";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class JobsService {


    private jobsURL = `${AppConfig.API_ENDPOINT}/jobs`;  // URL to web api

    constructor( private http: HttpClient) { }

    /** GET jobs from the server */
    getJobs(): Observable<Job[]> {
        return this.http.get<Job[]>(this.jobsURL);
    }


    getJob(id: string): Observable<Job> {
      const url = `${this.jobsURL}/${id}`;
      return this.http.get<Job>(url);
    }


    getClientJobs(id: number ): Observable<Job[]> {
      const url = `${this.jobsURL}/client/${id}`;
      return this.http.get<Job[]>(url);
    }


    /** POST: add a new job to the server */
    addJob(job: Job): Observable<Job> {
      const url = `${this.jobsURL}/add`;
      return this.http.post<Job>(url, job, httpOptions);
    }


    /** PUT: Update the client on the server */
    updateJob(job: Job): Observable<any> {
      const url = `${this.jobsURL}/${job.id}`;
      return this.http.post(url, job, httpOptions);
    }

    /** PUT: Update the invoice on the server */
    deleteJob(job: Job): Observable<any> {
        const url = `${this.jobsURL}/${job.id}`;
        return this.http.delete(url, httpOptions);
    }




}
