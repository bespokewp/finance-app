import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Client } from './models/client';
import { Job } from './models/job';
import { Injectable } from '@angular/core';
import { Invoice } from './models/invoice';

@Injectable({
    providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        const clients = [
            {
                id: 11,
                name: 'Carbon',
               work_value: 500,
                clientSince: 1549713996000,
                address: [
                    '99 Northerstraw',
                    '',
                    'Bristol',
                    'Linconshire',
                    'SN12 4PF',
                ],
                phoneNumber: '030 6745 2990'
            },
            {
                id: 12,
                name: 'ZY' ,
               work_value: 500,
                clientSince: 1549713996000,
                address: [
                    '99 Northerstraw',
                    '',
                    'Bristol',
                    'Linconshire',
                    'SN12 4PF',
                ],
                phoneNumber: '030 6745 2990'
            },
            {
                id: 13,
                name: 'Parrot' ,
               work_value: 500,
                clientSince: 1549713996000,
                address: [
                    '99 Northerstraw',
                    '',
                    'Bristol',
                    'Linconshire',
                    'SN12 4PF',
                ],
                phoneNumber: '030 6745 2990'
            },
            {
                id: 14,
                name: 'At-ease Media' ,
               work_value: 500,
                clientSince: 1549713996000,
                address: [
                    '99 Northerstraw',
                    '',
                    'Bristol',
                    'Linconshire',
                    'SN12 4PF',
                ],
                phoneNumber: '030 6745 2990'
            },
            {
                id: 15,
                name: 'Old Heat' ,
               work_value: 500,
                clientSince: 1549713996000,
                address: [
                    '99 Northerstraw',
                    '',
                    'Bristol',
                    'Linconshire',
                    'SN12 4PF',
                ],
                phoneNumber: '030 6745 2990'
            },
            {
                id: 16,
                name: 'Man-made media' ,
               work_value: 500,
                clientSince: 1549713996000,
                address: [
                    '99 Northerstraw',
                    '',
                    'Bristol',
                    'Linconshire',
                    'SN12 4PF',
                ],
                phoneNumber: '030 6745 2990'
            },
            {
                id: 17,
                name: 'Coffee pot',
               work_value: 500,
                clientSince: 1549713996000,
                address: [
                    '99 Northerstraw',
                    '',
                    'Bristol',
                    'Linconshire',
                    'SN12 4PF',
                ],
                phoneNumber: '030 6745 2990'
            }
        ];
        const jobs = [
            {
              id: 2,
              clientID: 11,
              client_name: 'Carbon',
              title: 'A new job',
              description: 'Description for a new job',
              dateAccepted: 1549788273000,
              dateDue: 1549888273000,
              costQuoted: 30000,
              completed: 0,
            },
            {
              id: 3,
              clientID: 12,
              client_name: 'Carbon',
              title: 'A new job 2',
              description: 'Description for a new job',
              dateAccepted: 1549788273000,
              dateDue: 1549888273000,
              costQuoted: 30000,
              completed: 0,
            },
            {
              id: 4,
              clientID: 12,
              client_name: 'Carbon',
              title: 'A new job 3',
              description: 'Description for a new job',
              dateAccepted: 1549788273000,
              dateDue: 1549888273000,
              costQuoted: 30000,
              completed: 0,
            },
            {
              id: 5,
              clientID: 14,
              client_name: 'At-ease Media',
              title: 'A new job 4',
              description: 'Description for a new job',
              dateAccepted: 1549788273000,
              dateDue: 1549888273000,
              costQuoted: 30000,
              completed: 0,
            }
        ];

        const invoices = [
          {
            id: 1,
            client: 14,
            client_name: 'At-ease Media',
            amount_due: 1000000,
            dateSent: 1549888273000,
            dateDue: 1549889273000,
            datePaid: 1549888773000,
            paid: false
          },
          {
            id: 2,
            client: 12,
            client_name: 'ZY',
            amount_due: 250000,
            dateSent: 1549988273000,
            dateDue: 1549989273000,
            datePaid: 1549988773000,
            paid: false
          },
          {
            id: 3,
            client: 12,
            client_name: 'ZY',
            amount_due: 100000,
            dateSent: 1549998273000,
            dateDue: 1549999273000,
            datePaid: 1549998773000,
            paid: true
          },
        ];
        return {clients, jobs, invoices};
    }

    // Overrides the genId method to ensure that a item always has an id.
    // If the items array is empty,
    // the method below returns the initial number (1).
    // if the item array is not empty, the method below returns the highest
    // item id + 1.
    genId<T extends Client | Job | Invoice>(items: T[]): number {
        return items.length > 0 ? Math.max(...items.map(item => item.id)) + 1 : 1;
    }
}
