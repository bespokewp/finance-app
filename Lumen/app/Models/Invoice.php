<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function jobs()
	{
		return $this->belongsToMany(Job::class);
	}
}
