<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function invoices()
	{
		return $this->belongsToMany(Invoice::class);
	}
}
