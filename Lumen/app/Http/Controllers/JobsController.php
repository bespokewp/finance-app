<?php

namespace App\Http\Controllers;

const DATE_FORMAT = 'Y-m-d';

use App\Models\Job;
use Illuminate\Http\Request;


class JobsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

	/**
	 * GET: /jobs
	 * Get all the jobs in the DB
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	function getJobs() {

		$jobs = Job::where('deleted', '=', false)->get();

		foreach ( $jobs as $job ) {
			$job->client;
			$job->invoices;
		}

		return $jobs;
	}


	/**
	 * GET: /jobs/{id}
	 * Get a job based on the ID in the URL
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	function getJob( $id ) {
		$job = Job::findOrFail( $id );
		$job->client;
		return $job;
	}


	/**
	 * POST: /jobs/client/{id}
	 * Return a list of jobs for a specific client
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	function getJobsForClient( $id ) {
		$jobs = Job::where( 'client_id', '=', $id )->get();
		return $jobs;
	}

	/**
	 * POST: /jobs/add
	 * Add a job to the DB
	 *
	 * @param Request $request
	 *
	 * @return Job
	 */
	function addJob( Request $request ) {
		/* Example json object parsed through
		 {
		  "clientID": "8",
		  "title": "job name",
		  "description": "job description",
		  "date_accepted": "2019-02-01",
		  "date_due": "2019-02-28",
		  "cost_quoted": 123.87
		}
		 */

		$job = new Job();
		$job->client_id = (int)$request->client_id;
		$job->title = $request->title;
		$job->description = $request->description;
		$job->date_accepted = date( DATE_FORMAT, strtotime( $request->date_accepted ) );
		$job->date_due = date( DATE_FORMAT, strtotime( $request->date_due ) );
		$job->cost_quoted = $request->cost_quoted * 100;

		$job->save();

		return $job;
	}

	/**
	 * POST: /jobs/{id}
	 *
	 * Update a jobs based on the ID in the URL
	 *
	 * @param Request $request
	 * @param $id
	 *
	 * @return mixed
	 */
	function updateJob( Request $request, $id ) {
		/* Example json object parsed through
		 {
			"title" : "A new title",
		    "id": 1,
		    "client_id": 1,
		    "date_sent": "1970-01-01",
		    "date_due": "2019-02-01",
		    "date_paid": "2019-02-01",
		    "paid": 0,
		    "created_at": "2019-02-15 10:04:19",
		    "updated_at": "2019-02-15 10:04:19"
		}
		 */

		$job = Job::findOrFail( $id );
		$job->title = $request->title;
		$job->description = $request->description;
		$job->date_accepted = date( DATE_FORMAT, strtotime( $request->date_accepted ) );
		$job->date_due = date( DATE_FORMAT, strtotime( $request->date_due ) );

		if ( isset ($request->completed ) ) {
			$job->cost_quoted = $request->cost_quoted;
			$job->completed = $request->completed;
		} else {
			$job->cost_quoted = $request->cost_quoted * 100;
		}

		$job->save();

		return $job;
	}

	/**
	 * DELETE: /jobs/add
	 * Mark a job as 'deleted', based on the ID in the URL
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	function deleteJob( $id ) {
		$job = Job::findOrFail( $id );
		$job->deleted = true;
		$job->save();

		return $job;
	}

}
