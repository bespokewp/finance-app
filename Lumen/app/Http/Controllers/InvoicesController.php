<?php

namespace App\Http\Controllers;

const DATE_FORMAT = 'Y-m-d';

use App\Models\Invoice;
use Illuminate\Http\Request;


class InvoicesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

	/**
	 * GET: /invoices
	 * Get all the invoices from the database
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	function getInvoices() {

		$invoices = Invoice::where('deleted', '=', false )->get();

		foreach ( $invoices as $key => $invoice ) {


			$invoice->client;
			$invoice->jobs;


			// pass the value of the work
			foreach ( $invoice->jobs as $job ) {
				$invoice->amount_billed += $job->cost_quoted;
				$job->display_amount = bcdiv($job->cost_quoted, 100, 2);
			}

			$invoice->display_amount = bcdiv( $invoice->amount_billed, 100, 2);

		}
		// show the amount in 2 decimal places

		return $invoices;
	}

	/**
	 * GET: /invoices/{id}
	 * Get an invoice based on the URL's ID parameter
	 * @param $id
	 *
	 * @return mixed
	 */
	function getInvoice( $id ) {
		$invoice =  Invoice::findOrFail( $id );
		$invoice->client;
		$invoice->amount_billed = 0;

		// pass the value of the work
		foreach ( $invoice->jobs as $job ) {
			$invoice->amount_billed += $job->cost_quoted;
			$job->display_amount = bcdiv($job->cost_quoted, 100, 2);
		}

		// show the amount in 2 decimal places
		$invoice->display_amount = bcdiv( $invoice->amount_billed, 100, 2);

		return $invoice;
	}

	/**
	 * POST: /invoices/add
	 * Take a parsed JSON object and add it to the database
	 * @param Request $request
	 *
	 * @return Invoice
	 */
	function addInvoice( Request $request ) {
		/* Example json object parsed through
		  {
			  "client": "8",
			  "jobs": [
			    1,
			    10,
			    11,
			    12
			  ],
			  "dateDue": "2019-01-31",
			  "dateSent": "2019-03-01"
		  }
		 */

		$invoice = new Invoice();
		$invoice->client_id = $request->client_id;
		$invoice->date_sent = $request->date_sent;
		$invoice->date_due = $request->date_due;
		$invoice->date_paid = $request->date_paid;

		$invoice->save();
		$jobs = $request->jobs;

		$invoice->jobs()->attach($jobs);

		return $invoice;
	}

	/**
	 * POST: /invoices/{id}
	 * Take a parsed invoice JSON object and update it
	 * @param Request $request
	 * @param $id
	 *
	 * @return mixed
	 */
	function updateInvoice( Request $request, $id ) {
		/* Example json object parsed through
		  {
			  "client": "8",
			  "jobs": [
			    1,
			    10,
			    11,
			    12
			  ],
			  "dateDue": "2019-01-31",
			  "dateSent": "2019-03-01"
		  }
		 */

		$invoice = Invoice::findOrFail( $id );

		if ( isset( $request->client_id) )
			$invoice->client_id = $request->client_id;

		if ( isset( $request->jobs ) && !empty( $request->jobs ) )
			$invoice->jobs()->sync( $request->jobs );
		elseif ( isset( $request->jobs ) && empty( $request->jobs ) )
			$invoice->jobs()->sync( [] );

		if ( isset( $request->date_due ) )
			$invoice->date_due = $request->date_due;

		if ( isset( $request->date_sent ) )
			$invoice->date_sent = $request->date_sent;

		if ( isset( $request->paid ) && $request->paid !== $invoice->paid  ) {
			$invoice->paid = $request->paid;
		}

		$invoice->save();

		return $invoice;
	}

	/**
	 * DELETE: /invoices/{id}
	 *
	 * Delete an invoice based on the URL's ID parameter
	 * @param $id
	 *
	 * @return mixed
	 */
	function deleteInvoice( $id ) {
		$invoice = Invoice::findOrFail( $id );
		$invoice->deleted = true;
		$invoice->save();
		return $invoice;
	}

}
