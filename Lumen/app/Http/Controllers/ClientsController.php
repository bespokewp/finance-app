<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

const DATE_FORMAT = 'Y-m-d';


class ClientsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

	/**
	 * GET: /clients
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	function getClients() {
		$clients = Client::where('deleted', '=', false)->get();

		foreach ( $clients as $client ) {
			$client->jobs;
			$client->value = 0;
			$client->address = unserialize($client->address);

			foreach ( $client->jobs as $job ) {
				$client->value += $job->cost_quoted;
			}

			$client->value /= 100;
		}

		return $clients;
	}

	/**
	 * GET: /clients/{id}
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	function getClient( $id ) {

		$client = \App\Models\Client::findOrFail( $id );
		$client->address = unserialize($client->address);

		$client->value = 0;

		foreach ( $client->jobs as $job ) {
			$client->value += $job->cost_quoted;
		}

		$client->value /= 100;

		return $client;
	}

	/**
	 * POST: /clients/add
	 * @param Request $request
	 *
	 * @return Client
	 */
	function addClient( Request $request ) {

		$client = new Client();
		$client->name = $request->name;
		$client->client_since = $request->client_since;
		$client->address = serialize($request->address);
		$client->phone_number = $request->phone_number;

		$client->save();

		return $client;

	}

	/**
	 * POST: /clients/{id}
	 * @param Request $request
	 * @param $id
	 *
	 * @return mixed
	 */
	function updateClient( Request $request, $id ) {

		$client = Client::findOrFail( $id );
		$client->name = $request->name;
		$client->address = serialize($request->address);
		$client->phone_number = $request->phone_number;

		$client->save();

		return $client;

	}

	/**
	 * DELETE: /invoices/{id}
	 *
	 * Delete an invoice based on the URL's ID parameter
	 * @param $id
	 *
	 * @return mixed
	 */
	function deleteClient( $id ) {
		$client = Client::findOrFail( $id );
		$client->deleted = true;
		$client->save();
		return $client;
	}

}
