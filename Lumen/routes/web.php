<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//////
/// Client routes
//////
	$router->get('/clients', 'ClientsController@getClients');
	$router->get('/clients/{id}', 'ClientsController@getClient');
	$router->post('/clients/add', 'ClientsController@addClient');
	$router->post('/clients/{id}', 'ClientsController@updateClient');


//////
/// Job routes
//////
	$router->get('/jobs', 'JobsController@getJobs');
	$router->get('/jobs/{id}', 'JobsController@getJob');
	$router->get('/jobs/client/{id}', 'JobsController@getJobsForClient');
	$router->post('/jobs/add', 'JobsController@addJob');
	$router->post('/jobs/{id}', 'JobsController@updateJob');
	$router->delete('/jobs/{id}', 'JobsController@deleteJob');



//////
/// Invoice routes
//////
	$router->get('/invoices', 'InvoicesController@getInvoices');
	$router->get('/invoices/{id}', 'InvoicesController@getInvoice');
	$router->post('/invoices/add', 'InvoicesController@addInvoice');
	$router->post('/invoices/{id}', 'InvoicesController@updateInvoice');
	$router->delete('/invoices/{id}', 'InvoicesController@deleteInvoice');
