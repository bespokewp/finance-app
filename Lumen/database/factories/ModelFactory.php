<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\Client::class, function (Faker $faker) {
	return [
		'name' => $faker->company,
		'client_since' => $faker->date('Y-m-d'),
		'address' => serialize([
			$faker->streetAddress,
			$faker->secondaryAddress,
			$faker->city,
			$faker->state,
			$faker->postcode,
		]),
		'phone_number' => $faker->phoneNumber,
	];
});

$factory->define(\App\Models\Job::class, function (Faker $faker) {
	return [
		'client_id' => mt_rand(1,10),
		'title' => $faker->text(15),
		'description' => $faker->text,
		'date_accepted' => $faker->date('Y-m-d'),
		'date_due' => $faker->date('Y-m-d'),
		'cost_quoted' => mt_rand(10000, 99999)
	];
});

$factory->define(\App\Models\Invoice::class, function (Faker $faker) {
	return [
		'client_id' => mt_rand(1,10),
		'date_sent' => $faker->date('Y-m-d'),
		'date_due' => $faker->date('Y-m-d'),
		'date_paid' => $faker->date('Y-m-d'),
		'paid' => mt_rand(0,1),
	];
});