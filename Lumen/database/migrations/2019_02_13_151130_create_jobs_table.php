<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->unsignedInteger( 'client_id' );
            $table->string( 'title', 50 );
            $table->text( 'description' )->nullable(true);
            $table->date( 'date_accepted' );
            $table->date( 'date_due' )->nullable(true);
            $table->unsignedInteger( 'cost_quoted' )->default(0);
	        $table->boolean('deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
