<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @ref [https://laravel.com/docs/5.7/seeding, https://laravel.com/docs/5.7/database-testing#writing-factories]
     * @return void
     */
    public function run()
    {

	    factory(\App\Models\Client::class, 10)->create();
	    factory(\App\Models\Job::class, 30)->create();
	    factory(\App\Models\Invoice::class, 5)->create();
    }
}
